import './App.scss';
import './assets/scss/reset.scss';
import './assets/scss/media.scss'
import MainContainer from './components/Pages/MainPage/MainPageContainer';
import { React } from 'react';

function App() {
  return (
    <div className="App">
      <MainContainer/>
    </div >
  );
}


export default App;
